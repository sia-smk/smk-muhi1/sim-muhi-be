<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class SiswaController extends Controller
{
    public function siswa() {
        $data = "Data All Siswa";
        return response()->json($data, 200);
    }

    public function siswaAuth() {
        $data = "Welcome " .  Auth::user()->name;
        return response()->json($data, 200);
    }
}
